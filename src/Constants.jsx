const workedMonths = "2 años";
const workedMonthsEn = "2 years";

//PROFILE TEXT - TEXTO DE PERFIL
const profileText = `Soy ingeniero de sistemas con experiencia en desarrollo web (Front-end) con conocimientos sólidos en Html5, css3, Sass, javascript, jquery, 
manejo y estructura de base de datos en mysql, manejo de versiones en Git. Experiencia en Php, Bootstrap 4, Semantic UI, Materialize
 y conocimiento avanzado en React js y react native. Tengo ${workedMonths} ejerciendo como desarrollador Front-end. 
 He estado al frente de varios proyectos variando entre landing pages y plataformas de administración. Soy una persona con buena capacidad lógica y aprendizaje rápido,
  tengo buena integración con el equipo de trabajo con muchas ganas de seguir aprendiendo y crecer como profesional.`;

const profileTextEn = `I'm front-end a developer with solid knowledgement in Html5, css3, Sass, javascript, jquery management and database structure in mysql, version management in Git. I have good experience in Php, Bootstrap 4, Semantic UI, Materialize
and advanced knowledge in React js and react native. I have ${workedMonthsEn} ​​working as a Front-end developer.
I have been in charge of several projects varying between landing pages and administration platforms. I'm a person with good logical ability and quick learning,
 I have good integration with the work team with a lot of desire to continue learning and grow as a professional.`;

//JOB EXPERIENCE - EXPERIENCIA LABORAL
const jobExperienceTitle = `Experiencia laboral (${workedMonths})`;
const jobExperienceTitleEn = `Work experience (${workedMonthsEn})`;
const jobExperienceData = [
  {
    title: "Desarrollador web en TejidoDigital, Barranquilla",
    description: "marzo 2019 - octubre 2020",
  },
  {
    title: "Analista de desarrollo en Red5g, Barranquilla",
    description: "octubre 2020 - actualmente",
  },
];

const jobExperienceDataEn = [
  {
    title: "Web developer in TejidoDigital, Barranquilla",
    description: "March 2019 - October 2020",
  },
  {
    title: "Development analyst in Red5g, Barranquilla",
    description: "October 2020 - Now",
  },
];

//EDUCATION - EDUCACION
const educationData = [
  {
    title:
      "Ingeniero de sistemas, Universidad Autónoma del Caribe, Barranquilla",
    description: "febrero 2015 - marzo 2020",
  },
];

const educationDataEn = [
  {
    title: "Systems Engineer, Autónoma del Caribe college, Barranquilla",
    description: "February 2015 - March 2020",
  },
];

//REFERENCIAS
const referenceData = [
  {
    title: "Ruben Cabrera Ricaurte - Analista de desarrollo en Serfinanza",
    description: "+57 3164186222",
  },
  {
    title:
      "Jostin Rojas Moreno - End user support specialist en Sykes Colombia S.A.S",
    description: "+57 3043788467",
  },
];

const referenceDataEn = [
  {
    title: "Ruben Cabrera Ricaurte - Development analyst in Serfinanza",
    description: "+57 3164186222",
  },
  {
    title:
      "Jostin Rojas Moreno - End user support specialist in Sykes Colombia S.A.S",
    description: "+57 3043788467",
  },
];

//CURSOS
const cursosData = [
  {
    title: "Certificado de Inglés avanzado C1, Cesfa",
    description: "enero 2012 - diciembre 2014",
  },
  {
    title: "React js - Udemy",
    description: "julio 2020",
  },
  {
    title: "React native - Udemy",
    description: "Mayo 2021",
  },
];

const cursosDataEn = [
  {
    title: "Advanced English Certificate C1, Cesfa",
    description: "January 2012 - December 2014",
  },
  {
    title: "React js - Udemy",
    description: "July 2020",
  },
  {
    title: "React native - Udemy",
    description: "May 2021",
  },
];

//DATOS PERSONALES
const personalData = [
  {
    title: "Correo",
    description: "aguilar.jdn@gmail.com",
    type: "email",
  },
  {
    title: "Teléfono",
    description: "+57 3002391674",
    type: "phone",
  },
  {
    title: "Dirección",
    description: "cra 28b #79-74 apartamento 202",
  },
  {
    title: "Linkedin",
    description:
      "https://www.linkedin.com/in/juan-david-nuñez-aguilar-92b729212/",
    type: "url",
  },
  {
    title: "Github",
    description: "https://github.com/le7els10",
    type: "url",
  },
];

const personalDataEn = [
  {
    title: "Email",
    description: "aguilar.jdn@gmail.com",
    type: "email",
  },
  {
    title: "Cellphone",
    description: "+57 3002391674",
    type: "phone",
  },
  {
    title: "Address",
    description: "cra 28b #79-74 apartamento 202",
  },
  {
    title: "Linkedin",
    description:
      "https://www.linkedin.com/in/juan-david-nuñez-aguilar-92b729212/",
    type: "url",
  },
  {
    title: "Github",
    description: "https://github.com/le7els10",
    type: "url",
  },
];

//Habilidades
const habilitiesData = [
  {
    title: "Css",
    value: 80,
  },
  {
    title: "Html5",
    value: 90,
  },
  {
    title: "Javascript",
    value: 85,
  },
  {
    title: "Jquery",
    value: 70,
  },
  {
    title: "Mysql",
    value: 80,
  },
  {
    title: "Php",
    value: 60,
  },
  {
    title: "React js",
    value: 75,
  },
  {
    title: "React native",
    value: 70,
  },
  {
    title: "Sass",
    value: 90,
  },
];
export {
  profileText,
  jobExperienceTitle,
  jobExperienceData,
  educationData,
  referenceData,
  cursosData,
  personalData,
  habilitiesData,
  profileTextEn,
  jobExperienceTitleEn,
  jobExperienceDataEn,
  educationDataEn,
  referenceDataEn,
  cursosDataEn,
  personalDataEn,
};
